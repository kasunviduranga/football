<?php 
  class players {
    // DB stuff
    private $conn;
    private $table = 'players';

    // players Properties
    public $id;
    public $id_terms;
    public $players_name;

    // Constructor with DB
    public function __construct($db) {
      $this->conn = $db;
    }

    // Create Players
    public function create() {
          // Create query
          $query = 'INSERT INTO ' . $this->table . ' SET id_terms = :team_name, players_name = :coach';

          // Prepare statement
          $stmt = $this->conn->prepare($query);

          // Clean data
          $this->id_terms = htmlspecialchars(strip_tags($this->id_terms));
          $this->players_name = htmlspecialchars(strip_tags($this->players_name));

          // Bind data
          $stmt->bindParam(':id_terms', $this->id_terms);
          $stmt->bindParam(':players_name', $this->players_name);

          // Execute query
          if($stmt->execute()) {
            return true;
      }

      // Print error if something goes wrong
      printf("Error: %s.\n", $stmt->error);

      return false;
    }

    // Get Players
    public function read() {
      // Create query
      $query = 'SELECT
        id,
        id_terms,
        players_name
      FROM
        ' . $this->table ;

      // Prepare statement
      $stmt = $this->conn->prepare($query);

      // Execute query
      $stmt->execute();

      return $stmt;
    }

    // Get Single Players
    public function read_single(){
    // Create query
    $query = 'SELECT
          id,
          id_terms,
          players_name
        FROM
          ' . $this->table . '
      WHERE id = ?
      LIMIT 0,1';

      //Prepare statement
      $stmt = $this->conn->prepare($query);

      // Bind ID
      $stmt->bindParam(1, $this->id);

      // Execute query
      $stmt->execute();

      $row = $stmt->fetch(PDO::FETCH_ASSOC);

      // set properties
      $this->id = $row['id'];
      $this->id_terms = $row['id_terms'];
      $this->players_name = $row['players_name'];
    }

    // Update Players
  public function update() {
    // Create Query
    $query = 'UPDATE ' .
      $this->table . '
    SET
    id_terms = :id_terms,
    players_name = :players_name
      WHERE
      id = :id';

  // Prepare Statement
  $stmt = $this->conn->prepare($query);

  // Clean data
  $this->players_name = htmlspecialchars(strip_tags($this->players_name));
  $this->id = htmlspecialchars(strip_tags($this->id));
  $this->id_terms = htmlspecialchars(strip_tags($this->id_terms));

  // Bind data
  $stmt-> bindParam(':players_name', $this->players_name);
  $stmt-> bindParam(':id', $this->id);
  $stmt-> bindParam(':id_terms', $this->id_terms);

  // Execute query
  if($stmt->execute()) {
    return true;
  }

  // Print error if something goes wrong
  printf("Error: $s.\n", $stmt->error);

  return false;
  }

  // Delete Players
  public function delete() {
    // Create query
    $query = 'DELETE FROM ' . $this->table . ' WHERE id = :id';

    // Prepare Statement
    $stmt = $this->conn->prepare($query);

    // clean data
    $this->id = htmlspecialchars(strip_tags($this->id));

    // Bind Data
    $stmt-> bindParam(':id', $this->id);

    // Execute query
    if($stmt->execute()) {
      return true;
    }

    // Print error if something goes wrong
    printf("Error: $s.\n", $stmt->error);

    return false;
  }
    
}