<?php 
  class teams {
    // DB stuff
    private $conn;
    private $table = 'teams';

    // teams Properties
    public $id;
    public $team_name;
    public $coach;

    // Constructor with DB
    public function __construct($db) {
      $this->conn = $db;
    }

    // Create teams
    public function create() {
          // Create query
          $query = 'INSERT INTO ' . $this->table . ' SET team_name = :team_name, coach = :coach';

          // Prepare statement
          $stmt = $this->conn->prepare($query);

          // Clean data
          $this->team_name = htmlspecialchars(strip_tags($this->team_name));
          $this->coach = htmlspecialchars(strip_tags($this->coach));

          // Bind data
          $stmt->bindParam(':team_name', $this->team_name);
          $stmt->bindParam(':coach', $this->coach);

          // Execute query
          if($stmt->execute()) {
            return true;
      }

      // Print error if something goes wrong
      printf("Error: %s.\n", $stmt->error);

      return false;
    }

    // Get teams
    public function read() {
      // Create query
      $query = 'SELECT
        id,
        coach,
        team_name
      FROM
        ' . $this->table ;

      // Prepare statement
      $stmt = $this->conn->prepare($query);

      // Execute query
      $stmt->execute();

      return $stmt;
    }

    // Get Single teams
    public function read_single(){
    // Create query
    $query = 'SELECT
          id,
          coach,
          team_name
        FROM
          ' . $this->table . '
      WHERE id = ?
      LIMIT 0,1';

      //Prepare statement
      $stmt = $this->conn->prepare($query);

      // Bind ID
      $stmt->bindParam(1, $this->id);

      // Execute query
      $stmt->execute();

      $row = $stmt->fetch(PDO::FETCH_ASSOC);

      // set properties
      $this->id = $row['id'];
      $this->coach = $row['coach'];
      $this->team_name = $row['team_name'];
    }

    // Update teams
  public function update() {
    // Create Query
    $query = 'UPDATE ' .
      $this->table . '
    SET
    coach = :coach,
    team_name = :team_name
      WHERE
      id = :id';

  // Prepare Statement
  $stmt = $this->conn->prepare($query);

  // Clean data
  $this->team_name = htmlspecialchars(strip_tags($this->team_name));
  $this->id = htmlspecialchars(strip_tags($this->id));
  $this->coach = htmlspecialchars(strip_tags($this->coach));

  // Bind data
  $stmt-> bindParam(':team_name', $this->team_name);
  $stmt-> bindParam(':id', $this->id);
  $stmt-> bindParam(':coach', $this->coach);

  // Execute query
  if($stmt->execute()) {
    return true;
  }

  // Print error if something goes wrong
  printf("Error: $s.\n", $stmt->error);

  return false;
  }

  // Delete teams
  public function delete() {
    // Create query
    $query = 'DELETE FROM ' . $this->table . ' WHERE id = :id';

    // Prepare Statement
    $stmt = $this->conn->prepare($query);

    // clean data
    $this->id = htmlspecialchars(strip_tags($this->id));

    // Bind Data
    $stmt-> bindParam(':id', $this->id);

    // Execute query
    if($stmt->execute()) {
      return true;
    }

    // Print error if something goes wrong
    printf("Error: $s.\n", $stmt->error);

    return false;
  }
  }