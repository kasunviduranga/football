<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: PUT');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

  include_once '../../config/Database.php';
  include_once '../../models/players.php';
  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  // Instantiate teams object
  $teams = new teams($db);

  // Get raw posted data
  $data = json_decode(file_get_contents("php://input"));

  // Set ID to UPDATE
  $teams->id = $data->id;
  $teams->coach = $data->coach;
  $teams->team_name = $data->team_name;

  // Update teams
  if($players->update()) {
    echo json_encode(
      array('message' => 'teams Updated')
    );
  } else {
    echo json_encode(
      array('message' => 'teams not updated')
    );
  }
