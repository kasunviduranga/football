<?php

  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../../config/Database.php';
  include_once '../../models/players.php';
  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();
  // Instantiate teams object
  $teams = new teams($db);

  // Get ID
  $teams->id = isset($_GET['id']) ? $_GET['id'] : die();

  // Get teams
  $teams->read_single();

  // Create array
  $teams_arr = array(
    'id' => $teams->id,
    'coach' => $teams->coach,
    'team_name' => $teams->team_name
  );

  // Make JSON
  print_r(json_encode($teams_arr));
