<?php 
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../../config/Database.php';
  include_once '../../models/players.php';

  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  // Instantiate teams object
  $teams = new teams($db);

  // teams read query
  $result = $teams->read();
  
  // Get row count
  $num = $result->rowCount();

  // Check if any teams
  if($num > 0) {
        // teams array
        $teams_arr = array();
        $teams_arr['data'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)) {
          extract($row);

          $teams_item = array(
            'id' => $teams->id,
            'coach' => $teams->coach,
            'team_name' => $teams->team_name
          );

          // Push to "data"
          array_push($teams_arr['data'], $teams_item);
        }

        // Turn to JSON & output
        echo json_encode($teams_arr);

  } else {
        // No teams
        echo json_encode(
          array('message' => 'No teams Found')
        );
  }
