<?php

  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../../config/Database.php';
  include_once '../../models/players.php';
  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();
  // Instantiate players object
  $players = new players($db);

  // Get ID
  $players->id = isset($_GET['id']) ? $_GET['id'] : die();

  // Get players
  $players->read_single();

  // Create array
  $players_arr = array(
    'id' => $players->id,
    'id_terms' => $players->id_terms,
    'players_name' => $players->players_name
  );

  // Make JSON
  print_r(json_encode($players_arr));
