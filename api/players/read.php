<?php 
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../../config/Database.php';
  include_once '../../models/players.php';

  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  // Instantiate players object
  $players = new players($db);

  // players read query
  $result = $players->read();
  
  // Get row count
  $num = $result->rowCount();

  // Check if any players
  if($num > 0) {
        // players array
        $players_arr = array();
        $players_arr['data'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)) {
          extract($row);

          $players_item = array(
            'id' => $players->id,
            'id_terms' => $players->id_terms,
            'players_name' => $players->players_name
          );

          // Push to "data"
          array_push($players_arr['data'], $players_item);
        }

        // Turn to JSON & output
        echo json_encode($players_arr);

  } else {
        // No players
        echo json_encode(
          array('message' => 'No players Found')
        );
  }
