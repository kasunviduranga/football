<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: DELETE');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');
 
  include_once '../../config/Database.php';
  include_once '../../models/players.php';
  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  // Instantiate players object
  $players = new players($db);

  // Get raw posted data
  $data = json_decode(file_get_contents("php://input"));

  // Set ID to UPDATE
  $players->id = $data->id;

  // Delete players
  if($players->delete()) {
    echo json_encode(
      array('message' => 'players deleted')
    );
  } else {
    echo json_encode(
      array('message' => 'players not deleted')
    );
  }
