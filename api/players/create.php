<?php 
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

  include_once '../../config/Database.php';
  include_once '../../models/players.php';

  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  // Instantiate blog post object 
  $players = new players($db);

  // Get raw posted data 
  $data = json_decode(file_get_contents("php://input")); 

  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://heisenbug-premier-league-live-scores-v1.p.rapidapi.com/api/premierleague/playerdetails?player=Mohamed%20Salah&team=Liverpool",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      "x-rapidapi-host: heisenbug-premier-league-live-scores-v1.p.rapidapi.com",
      "x-rapidapi-key: 1046cb1b7cmsh69d735c03d75460p15a305jsn2998f0aa280d"
    ),
  ));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	$response= json_decode($response, true);
	// var_dump($response["matches"][0]);
	for($i=0;$i<count($response);$i++){
		$players->id_terms = $response["playerName"];
    $players->players_name = $response["playerName"];

    // Create post
    if($players->create()) {
      echo json_encode(
        array('message' => 'Created')
      );
    } else {
        echo json_encode(
          array('message' => 'Not Created')
      );
    }
  }
}